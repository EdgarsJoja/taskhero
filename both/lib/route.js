/*=========== Global ===============*/
FlowRouter.triggers.exit([function(context, redirect) {
    amplify.store('routes', {
        current: FlowRouter.current().path
    });
}]);
/*=========== Groups ===============*/

var hero = FlowRouter.group({
    prefix: '/hero'
});

/*============ Routes =============*/

FlowRouter.route('/', {
    action: function () {
        FlowRouter.go('/hero/choose');
    }
});

/* ----------- Hero -------*/

/* Choose */
hero.route('/choose', {
    action: function (params, queryParams) {
        BlazeLayout.render('template_hero_choose_index');
    }
});

hero.route('/create', {
    action: function (params, queryParams) {
        BlazeLayout.render('template_hero_create_index');
    }
});

hero.route('/', {
    action: function (params, queryParams) {
        BlazeLayout.render('template_hero_stats_index');
    }
});

hero.route('/quests', {
    action: function (params, queryParams) {
        BlazeLayout.render('template_hero_quests_index');
    }
});

hero.route('/shop', {
    action: function (params, queryParams) {
        BlazeLayout.render('template_hero_shop_index');
    }
});

hero.route('/quests/create', {
    action: function (params, queryParams) {
        BlazeLayout.render('template_hero_quests_create_index');
    }
});

/*-------- About page ----------*/

FlowRouter.route('/about', {
    action: function (params, queryParams) {
        BlazeLayout.render('template_app_about_index');
    }
});