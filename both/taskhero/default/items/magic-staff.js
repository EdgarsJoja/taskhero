/**
 *
 * @param options
 * @constructor
 */
MagicStaff = function MagicStaff(options) {
    this.options = extend({
        type: ITEM_TYPE_PERMANENT,
        name: 'Magic Staff',
        cost: 1,
        weight: 3,
        target: ITEM_TARGET_HERO,
        description: 'Turns 20 xp into 5 gold'
    }, options);

    this.obj = {};
};

/**
 *
 * @type {Item}
 */
MagicStaff.prototype = new Item();

/**
 *
 * @type {MagicStaff}
 */
MagicStaff.prototype.constructor = MagicStaff;

/**
 *
 * @param id
 */
MagicStaff.prototype.use = function (id) {
    Item.prototype.use.call(this, id);
};

/**
 *
 * @param id
 */
MagicStaff.prototype.apply = function (id) {
    var hero = getHero(id);

    if (hero.xp >= 20) {
        Heroes.update(
            {_id: id},
            {
                $set: {
                    xp: hero.xp - 20,
                    gold: hero.gold + 5
                }
            }
        );
    } else {
        alert("You must have at least 20 xp!");
    }
};
