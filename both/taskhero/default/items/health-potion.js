/**
 * Constructor
 *
 * @param options
 * @constructor
 */
HealthPotion = function HealthPotion(options) {
    this.options = extend({
        type: ITEM_TYPE_CONSUMABLE,
        name: 'Health potion',
        cost: 8,
        weight: 1,
        target: ITEM_TARGET_HERO,
        description: 'Heals one health'
    }, options);

    this.obj = {};
};

/**
 *
 * @type {Item}
 */
HealthPotion.prototype = new Item;

/**
 *
 * @type {HealthPotion}
 */
HealthPotion.prototype.constructor = HealthPotion;

/**
 * Item use function
 *
 * @param id
 */
HealthPotion.prototype.use = function (id) {
    Item.prototype.use.call(this, id);
};

/**
 * Apply effects
 *
 * @param id
 */
HealthPotion.prototype.apply = function (id) {
    var newHealth = this.obj.stats.health + 1;

    Heroes.update(
        {_id: id},
        {
            $set: {
                stats: {
                    strength: this.obj.stats.strength,
                    intelligence: this.obj.stats.intelligence,
                    health: newHealth,
                    charisma: this.obj.stats.charisma
                }
            }
        }
    );
};
