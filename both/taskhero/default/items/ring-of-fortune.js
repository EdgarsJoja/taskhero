/**
 *
 * @param options
 * @constructor
 */
RingOfFortune = function RingOfFortune(options) {
    this.options = extend({
        type: ITEM_TYPE_CONSUMABLE,
        name: 'Ring of Fortune',
        cost: 15,
        weight: 2,
        target: ITEM_TARGET_HERO,
        description: 'Adds 1 skill point'
    }, options);

    this.obj = {};
};

/**
 *
 * @type {Item}
 */
RingOfFortune.prototype = new Item();

/**
 *
 * @type {RingOfFortune}
 */
RingOfFortune.prototype.constructor = RingOfFortune;

/**
 *
 * @param id
 */
RingOfFortune.prototype.use = function (id) {
    Item.prototype.use.call(this, id);
};

/**
 *
 * @param id
 */
RingOfFortune.prototype.apply = function (id) {
    var hero = getHero(id);

    Heroes.update(
        {_id: id},
        {
            $set: {
                sp: hero.sp + 1
            }
        }
    );
};
