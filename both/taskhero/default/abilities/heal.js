/**
 *
 * @param options
 * @constructor
 */
Heal = function Heal(options) {
    this.options = extend({
        id: null,
        target: ABILITY_TARGET_HERO,
        type: ABILITY_TYPE_ACTIVE,
        cost: 2,
        activate_cost: {intelligence: 1, strength: 1},
        name: 'Heal',
        description: 'Adds 1 health. Activation requires 1 intelligence and 1 strength.'
    }, options);

    this.obj = {};
};

/**
 *
 * @type {Ability}
 */
Heal.prototype = new Ability;

/**
 *
 * @type {Heal}
 */
Heal.prototype.constructor = Heal;

/**
 *
 * @param id
 */
Heal.prototype.use = function (id) {
    Ability.prototype.use.call(this, id);
};

/**
 *
 * @param id
 */
Heal.prototype.apply = function (id) {
    var hero = getHero(id);

    Heroes.update(
        {_id: id},
        {
            $set: {
                stats: {
                    strength: hero.stats.strength,
                    intelligence: hero.stats.intelligence,
                    health: hero.stats.health + 1,
                    charisma: hero.stats.charisma
                }
            }
        }
    );
};