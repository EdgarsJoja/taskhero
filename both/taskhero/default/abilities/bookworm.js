/**
 *
 * @param options
 * @constructor
 */
Bookworm = function Bookworm(options) {
    this.options = extend({
        id: null,
        target: ABILITY_TARGET_HERO,
        type: ABILITY_TYPE_ACTIVE,
        cost: 5,
        activate_cost: {health: 1},
        name: 'Bookworm',
        description: 'Adds 5 xp for 1 health.'
    }, options);

    this.obj = {};
};

/**
 *
 * @type {Ability}
 */
Bookworm.prototype = new Ability;

/**
 *
 * @type {Bookworm}
 */
Bookworm.prototype.constructor = Bookworm;

/**
 *
 * @param id
 */
Bookworm.prototype.use = function (id) {
    Ability.prototype.use.call(this, id);
};

/**
 *
 * @param id
 */
Bookworm.prototype.apply = function (id) {
    var hero = getHero(id);

    Heroes.update(
        {_id: id},
        {
            $set: {
                xp: hero.xp + 5
            }
        }
    );
};