stats = {
    knight: {
        strength: 5,
        intelligence: 3,
        health: 3,
        charisma: 3
    },
    scientist: {
        strength: 3,
        intelligence: 5,
        health: 3,
        charisma: 3
    },
    druid: {
        strength: 3,
        intelligence: 3,
        health: 5,
        charisma: 3
    },
    adventurer: {
        strength: 3,
        intelligence: 3,
        health: 3,
        charisma: 5
    }
};