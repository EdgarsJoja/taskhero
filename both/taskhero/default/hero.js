levels = {
    1: {xp: 0, sp: 1},
    2: {xp: 3, sp: 1},
    3: {xp: 12, sp: 1},
    4: {xp: 20, sp: 1},
    5: {xp: 30, sp: 3},
    6: {xp: 50, sp: 1},
    7: {xp: 85, sp: 1},
    8: {xp: 100, sp: 1},
    9: {xp: 120, sp: 1},
    10: {xp: 150, sp: 5},
    11: {xp: 190, sp: 1},
    12: {xp: 240, sp: 1},
    13: {xp: 300, sp: 1}
};

/**
 * Return next level by xp
 *
 * @param xp
 * @returns {string}
 */
getNextLevelByXp = function (xp) {
    for (var lvl in levels) {
        if (levels.hasOwnProperty(lvl) && levels[lvl].xp > xp) {
            return levels[lvl];
        }
    }
};

/**
 *
 * Get next level
 *
 * @param lvl
 * @returns {*}
 */
getNextLevel = function (lvl) {
    if (levels.hasOwnProperty(lvl)) {
        return levels[++lvl];
    }
};
