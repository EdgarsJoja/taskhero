/**
 * Item can be used many times
 *
 * @type {number}
 */
ITEM_TYPE_PERMANENT = 1;

/**
 * Item disappears after using
 *
 * @type {number}
 */
ITEM_TYPE_CONSUMABLE = 2;

/**
 * Item effect targets hero
 *
 * @type {number}
 */
ITEM_TARGET_HERO = 1;

/**
 * Item effect targets quest
 *
 * @type {number}
 */
ITEM_TARGET_QUEST = 2;

/**
 * Item constructor
 *
 * @param options
 * @constructor
 */
Item = function Item(options) {
    this.options = extend({
        id: null,
        type: 'item',
        name: 'item',
        cost: 1,
        weight: 0,
        target: ITEM_TARGET_HERO,
        description: 'Items can be used '
    }, options);

    this.obj = {};
};

/**
 * Item use function, checks for requirements
 *
 * @param id
 */
Item.prototype.use = function (id) {
    switch (this.options.target) {
        case ITEM_TARGET_HERO:
            this.obj = Heroes.findOne(id, {});
            break;
        case ITEM_TARGET_QUEST:
            this.obj = Quests.findOne(id, {});
            break;
        default:
            this.obj = {};
            break;
    }

    if (this.obj && this.options.id) {
        if (this.options.type == ITEM_TYPE_CONSUMABLE) {
            Items.remove({_id: this.options.id});
        }

        this.apply(id);
    }
};

/**
 * Apply item effects. OVERRIDE this for all item effects to heroes, quests!
 *
 * @param id
 */
Item.prototype.apply = function (id) {

};
