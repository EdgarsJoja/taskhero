/**
 *
 * @returns {Array}
 */
createShopItems = function () {
    var items = [];

    items.push(new HealthPotion);
    items.push(new MagicStaff);
    items.push(new RingOfFortune);

    return items;
};

/**
 *
 * @returns {Array}
 */
createShopAbilities = function () {
    var abilities = [];

    abilities.push(new Heal);
    abilities.push(new Bookworm);

    return abilities;
};