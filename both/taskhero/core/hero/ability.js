/**
 * Ability effect is working all the time
 *
 * @type {number}
 */
ABILITY_TYPE_PASSIVE = 1;

/**
 * Ability can be used only momentarily
 *
 * @type {number}
 */
ABILITY_TYPE_ACTIVE = 2;

/**
 * Ability targets hero
 *
 * @type {number}
 */
ABILITY_TARGET_HERO = 1;

/**
 * Ability targets quest
 *
 * @type {number}
 */
ABILITY_TARGET_QUEST = 2;

/**
 * Ability class constructor
 *
 * @param options
 * @constructor
 */
Ability = function (options) {
    this.options = extend({
        id: null,
        target: ABILITY_TARGET_HERO,
        type: 'ability',
        cost: 1,
        activate_cost: {strength: 1},
        name: 'ability',
        description: 'Abilities can be bought/activated using xp and strength'
    }, options);

    this.obj = {};
};

/**
 * Ability use function
 *
 * @param id
 */
Ability.prototype.use = function (id) {
    switch (this.options.target) {
        case ITEM_TARGET_HERO:
            this.obj = getHero(id);
            break;
        case ITEM_TARGET_QUEST:
            this.obj = getQuest(id);
            break;
        default:
            this.obj = {};
            break;
    }

    if (this.obj && this.options.id) {
        var activateCost = this.options.activate_cost,
            newStats = this.obj.stats,
            canUse = true;

        for (var key in activateCost) {
            if (activateCost.hasOwnProperty(key)) {
                if (this.obj.stats[key] < activateCost[key]) {
                    canUse = false;
                    break;
                } else {
                    newStats[key] -= activateCost[key];
                }
            }
        }

        if (canUse) {
            Heroes.update(
                {_id: this.obj._id},
                {
                    $set: {
                        stats: newStats
                    }
                }
            );

            this.apply(id);
        } else {
            alert("Ability activation parameters are not valid");
        }
    }
};

/**
 * Apply ability effect. OVERRIDE this for abilities functionality.
 *
 * @param id
 */
Ability.prototype.apply = function (id) {

};
