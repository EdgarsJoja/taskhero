/**
 * Easy difficulty
 *
 * @type {number}
 */
QUEST_DIFFICULTY_EASY = 1;

/**
 * Medium difficulty
 *
 * @type {number}
 */
QUEST_DIFFICULTY_MEDIUM = 2;

/**
 * Hard difficulty
 *
 * @type {number}
 */
QUEST_DIFFICULTY_HARD = 3;