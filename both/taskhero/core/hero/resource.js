/**
 * For each x charisma character will get additionally 1 gold on quest complete
 *
 * @type {number}
 */
ADDITIONAL_GOLD_FOR_CHARISMA = 5;

/**
 * Update hero attributes on quest complete
 *
 * @param heroId
 * @param questId
 */
updateHeroOnQuestComplete = function (heroId, questId) {
    var hero = getHero(heroId),
        quest = getQuest(questId);

    var gold = hero.gold,
        xp = hero.xp,
        level = hero.level,
        sp = hero.sp;

    /**
     * Give gold for completing quest
     * @type {number}
     */
    gold += getGoldByDiff(quest.difficulty) + hero.stats.charisma / ADDITIONAL_GOLD_FOR_CHARISMA;

    /**
     * Give xp for completing quest
     * @type {number}
     */
    xp += getXpByDiff(quest.difficulty);

    /**
     * Check if level increased
     */
    for (var lvl in levels) {
        if (levels.hasOwnProperty(lvl) && xp >= levels[lvl].xp && level < lvl) {
            level = lvl;
            sp += levels[lvl].sp;
        }
    }

    Heroes.update(
        {_id: heroId},
        {
            $set: {
                gold: gold,
                xp: xp,
                level: level,
                sp: sp
            }
        }
    );
};
