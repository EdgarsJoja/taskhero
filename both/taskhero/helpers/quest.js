/**
 * Get Quest by id
 *
 * @param id
 * @returns {137|776|*|Object}
 */
getQuest = function(id) {
    return Quests.findOne(id, {});
};

/**
 * Return gold that hero can get by completing quest
 *
 * @param diff
 * @returns {number}
 */
getGoldByDiff = function(diff) {
    if (diff == QUEST_DIFFICULTY_EASY) {
        return 1;
    } else if (diff == QUEST_DIFFICULTY_MEDIUM) {
        return 3;
    } else if (diff == QUEST_DIFFICULTY_HARD) {
        return 5;
    }
};

/**
 * Get xp that hero receives when completing quest
 *
 * @param diff
 * @returns {number}
 */
getXpByDiff = function(diff) {
    switch (diff) {
        case QUEST_DIFFICULTY_EASY.toString():
            return 1;
            break;
        case QUEST_DIFFICULTY_MEDIUM.toString():
            return 3;
            break;
        case QUEST_DIFFICULTY_HARD.toString():
            return 5;
            break;
        default:
            return 0;
            break;
    }
};

/**
 * Get quest end dates
 *
 * @returns {{}}
 */
getDates = function() {
    var dates = {},
        now = new Date();

    dates['today'] = new Date();
    dates['today'].setHours(24, 0, 0, 0);
    dates['today'] = dates['today'].getTime();

    dates['tomorrow'] = new Date();
    dates['tomorrow'].setHours(48, 0, 0, 0);
    dates['tomorrow'] = dates['tomorrow'].getTime();

    dates['week'] = new Date(now.setDate(now.getDate() - now.getDay() + 8));
    dates['week'].setHours(0, 0, 0, 0);
    dates['week'] = dates['week'].getTime();

    dates['month'] = new Date(now.getFullYear(), now.getMonth() + 1);
    dates['month'] = dates['month'].getTime();

    return dates;
};
