/**
 * get hero by id from MongoDB
 *
 * @param id
 * @returns {137|776|*|Object}
 */
getHero = function (id) {
    return Heroes.findOne(id, {});
};

/**
 * Get current weight from items for hero
 * @param id
 * @returns {number}
 */
getHeroItemsWeight = function (id) {
    var items = Items.find({hero: id}),
        weight = 0;

    items.forEach(function (item) {
        weight += item.weight;
    });

    return weight;
};