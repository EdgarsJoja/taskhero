App.info({
    name: 'TaskHero',
    description: 'ToDo app with game elements',
    version: '0.0.1'
});

App.icons({
    'android_ldpi': 'public/images/icon-ldpi.png',
    'android_mdpi': 'public/images/icon-mdpi.png',
    'android_hdpi': 'public/images/icon-hdpi.png',
    'android_xhdpi': 'public/images/icon-xhdpi.png'
});