/**
 * Hero creation form page events
 */
Template.template_hero_create_index.events({

    /**
     * Create new hero
     *
     * @param event
     * @returns {boolean}
     */
    'submit #form-create-hero': function (event) {
        event.preventDefault();

        var name = event.target.name.value,
            characterClass = event.target.class.value;

        if (!name.trim() || !characterClass.trim()) {
            alert('Please fill all fields.');
            return false;
        }

        Heroes.insert({
            name: event.target.name.value,
            class: event.target.class.value,
            stats: window['stats'][event.target.class.value.toLowerCase()],
            xp: 0,
            gold: 5,
            level: 1,
            sp: 0
        });

        event.target.reset();

        FlowRouter.go('/');
    }
});