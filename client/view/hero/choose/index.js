
Template.template_hero_choose_index.events({

    'click .hero': function(event) {
        var target = $(event.currentTarget),
            options = target.children('.options-wrapper');

        options.slideToggle();
    },

    'click .option': function (event) {
        event.preventDefault();

        var target = $(event.target);

        if (target.hasClass('select')) {
            amplify.store('hero', {
                id: this._id
            });

            FlowRouter.go('/hero');
        } else if (target.hasClass('delete')) {
            if (confirm('Delete hero ' + this.name + '?')) {
                Heroes.remove(this._id);
            }
        }
    }
});

Template.template_hero_choose_index.helpers({
    'heroes': function () {
        return Heroes.find().fetch();
    }
});