/**
 * Stats template events
 */
Template.template_hero_stats_index.events({

    /**
     *
     * @param event
     */
    'click .title': function (event) {
        var title = $(event.target),
            details = title.next('.details');

        details.slideToggle();
    },

    /**
     *
     * @param event
     */
    'click .option': function (event) {
        var opt = $(event.target);

        if (opt.hasClass('use') && confirm('Use ' + this.options.name + '?')) {
            this.use(amplify.store('hero').id);
        }
    },

    /**
     *
     * @param event
     */
    'click .switch button': function (event) {
        var target = $(event.target),
            btns = $('.switch button'),
            lists = $('.inventory .list');

        btns.each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
        });

        target.addClass('active');

        lists.each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
        });

        $('.' + target.attr('data-target')).addClass('active');
    }
});

/**
 * Helpers in template
 */
Template.template_hero_stats_index.helpers({

    /**
     *
     * @returns {137|*|Object|776}
     */
    'hero': function () {
        return Heroes.findOne(amplify.store('hero').id);
    },

    /**
     *
     * @returns {Array}
     */
    'items': function () {
        var items = Items.find({hero: amplify.store('hero').id}),
            itemObjects = [];

        items.forEach(function (item) {
            itemObjects.push(new window[item.item]({id: item._id}));
        });

        return itemObjects;
    },

    /**
     *
     * @returns {Array}
     */
    'abilities': function () {
        var abilities = Abilities.find({hero: amplify.store('hero').id}),
            abilitiesObjects = [];

        abilities.forEach(function (ability) {
            abilitiesObjects.push(new window[ability.ability]({id: ability._id}));
        });

        return abilitiesObjects;
    },

    /**
     * Get next level xp
     *
     * @returns {*}
     */
    'nextLevelXp': function () {
        var hero = Heroes.findOne({_id: amplify.store('hero').id});

        if (hero) {
            var level = getNextLevel(hero.level);

            if (level) {
                return level.xp;
            }
        }

        return '*';
    }
});