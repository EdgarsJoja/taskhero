/**
 * Template events
 */
Template.template_hero_quests_create_index.events({

    /**
     *
     * @param event
     * @returns {boolean}
     */
    'submit #form-create-quest': function (event) {
        event.preventDefault();

        var title = event.target.title.value,
            quest = FlowRouter.getQueryParam('quest');

        if (!title.trim()) {
            alert('Please fill required fields.');
            return false;
        }

        if (!quest) {
            Quests.insert({
                hero: amplify.store('hero').id,
                title: title,
                description: event.target.description.value,
                difficulty: event.target.difficulty.value,
                status: 'todo',
                time: event.target.time.value
            });
        } else {
            Quests.update(
                {_id: quest._id},
                {
                    $set: {
                        title: title,
                        description: event.target.description.value,
                        difficulty: event.target.difficulty.value,
                        time: event.target.time.value
                    }
                }
            );
        }

        event.target.reset();

        FlowRouter.go('/hero/quests');
    }
});

/**
 * Template helpers
 */
Template.template_hero_quests_create_index.helpers({

    /**
     *
     * @returns {number}
     */
    'dates': function () {
        return getDates();
    }
});

/**
 * Setting values for edit form
 */
Template.template_hero_quests_create_index.onRendered(function () {

    var quest = FlowRouter.getQueryParam('quest');

    if (quest) {
        var title = $('[name=\'title\']'),
            description = $('[name=\'description\']'),
            difficulty = $('[name=\'difficulty\']'),
            time = $('[name=\'time\']');

        title.val(quest.title);
        description.val(quest.description);
        difficulty.val(quest.difficulty);
        time.val(quest.time);
    }
});