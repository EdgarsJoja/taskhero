/**
 * Template events
 */
Template.template_hero_quests_index.events({

    /**
     *
     * @param event
     */
    'click .title': function (event) {
        var title = $(event.target),
            details = title.next('.details');

        details.slideToggle();
    },

    /**
     *
     * @param event
     */
    'click .option': function (event) {
        var target = $(event.target);

        if (confirm('Are you sure?')) {
            if (target.hasClass('done') && this.status != 'done') {
                Quests.update(
                    {_id: this._id},
                    {$set: {status: 'done'}}
                );

                updateHeroOnQuestComplete(this.hero, this._id);
            } else if (target.hasClass('todo') && this.status != 'todo') {
                Quests.update(
                    {_id: this._id},
                    {$set: {status: 'todo'}}
                );
            } else if (target.hasClass('edit')) {
                FlowRouter.go('/hero/quests/create', null, {quest: this});
            } else if (target.hasClass('delete')) {
                Quests.remove(this._id);
            }
        }
    }
});

/**
 * Template helpers
 */
Template.template_hero_quests_index.helpers({

    /**
     *
     * @returns {*}
     */
    'quests': function () {
        return Quests.find({hero: amplify.store('hero').id});
    },

    /**
     *
     * @returns {*}
     */
    'quests_today': function () {
        var today = getDates().today.toString();

        return Quests.find({time: {$lte: today}, hero: amplify.store('hero').id}).fetch();
    },

    /**
     *
     * @returns {*}
     */
    'quests_tomorrow': function () {
        var tomorrow = getDates().tomorrow.toString(),
            today = getDates().today.toString();

        return Quests.find({time: {$lte: tomorrow, $gt: today}, hero: amplify.store('hero').id}).fetch();
    },

    /**
     *
     * @returns {*}
     */
    'quests_week': function () {
        var week = getDates().week.toString(),
            tomorrow = getDates().tomorrow.toString();

        return Quests.find({time: {$lte: week, $gt: tomorrow}, hero: amplify.store('hero').id}).fetch();
    },

    /**
     *
     * @returns {*}
     */
    'quests_month': function () {
        var month = getDates().month.toString(),
            week = getDates().week.toString();

        return Quests.find({time: {$lte: month, $gt: week}, hero: amplify.store('hero').id}).fetch();
    }
});