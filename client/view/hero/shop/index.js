/**
 * Template events
 */
Template.template_hero_shop_index.events({

    /**
     *
     * @param event
     */
    'click .title': function (event) {
        var title = $(event.target),
            details = title.next('.details');

        details.slideToggle();
    },

    /**
     * Items
     *
     * @param event
     */
    'click .item .option': function (event) {
        var opt = $(event.target),
            hero = Heroes.findOne(amplify.store('hero').id),
            weight = getHeroItemsWeight(hero._id);

        if (opt.hasClass('buy') && hero.gold >= this.options.cost &&
            (hero.stats.strength * 2 >= weight + this.options.weight) && confirm('Buy ' + this.options.name + '?')
        ) {

            /**
             * Payment
             */
            Heroes.update(
                {_id: hero._id},
                {$set: {gold: hero.gold - this.options.cost}}
            );

            Items.insert({
                hero: amplify.store('hero').id,
                item: this.constructor.name,
                weight: this.options.weight
            });
        } else if (hero.gold < this.options.cost) {
            alert('You don\'t have enough gold');
        } else if (hero.stats.strength * 2 < weight + this.options.weight) {
            alert('You need more strength to carry this item');
        }
    },

    /**
     * Abilities
     *
     * @param event
     */
    'click .ability .option': function (event) {
        var opt = $(event.target),
            hero = Heroes.findOne(amplify.store('hero').id),
            hasAbility = Abilities.find({hero: hero._id, ability: this.constructor.name}).fetch().length;

        if (opt.hasClass('buy') && hero.sp >= this.options.cost && !hasAbility && confirm('Buy ' + this.options.name + '?')) {

            /**
             * Payment
             */
            Heroes.update(
                {_id: hero._id},
                {$set: {sp: hero.sp - this.options.cost}}
            );

            Abilities.insert({
                hero: amplify.store('hero').id,
                ability: this.constructor.name
            });
        } else if (hasAbility) {
            alert('You already have this ability!');
        } else if (hero.sp < this.options.cost) {
            alert('You don\'t have enough skill points');
        }
    }
});

/**
 * Template helpers
 */
Template.template_hero_shop_index.helpers({

    /**
     *
     * @returns {Array}
     */
    'items': function () {
        return createShopItems();
    },

    /**
     *
     * @returns {Array}
     */
    'abilities': function () {
        return createShopAbilities();
    }
});