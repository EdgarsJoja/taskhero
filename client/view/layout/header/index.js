Template.template_layoutheader_index.events({
    'click .back': function () {
        FlowRouter.go(amplify.store('routes').current);
    },

    'click .menu': function (event) {
        var navigation = $('.menu-list');

        navigation.slideToggle();
    }
});
